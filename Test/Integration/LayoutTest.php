<?php

namespace Pixelpromis\NiceAdmin\Test\Integration;

use Magento\Framework\View\Page\Config as PageConfig;
use Magento\TestFramework\TestCase\AbstractBackendController;

class LayoutTest extends AbstractBackendController
{

	public function testCssFileIsLoaded()
	{
		$this->dispatch('backend/admin/dashboard/index');

		/** @var PageConfig $pageConfig */
		$pageConfig = $this->_objectManager->get(PageConfig::class);

		$this->assertArrayHasKey('Pixelpromis_NiceAdmin::css/pixelpromis.css', $pageConfig->getAssetCollection()->getAll());
	}

}