<?php

namespace Pixelpromis\NiceAdmin\Test\Integration;

use Magento\Framework\App\DeploymentConfig;
use Magento\Framework\Component\ComponentRegistrar;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Module\ModuleList;
use Magento\TestFramework\ObjectManager;

class ModuleConfigTest extends \PHPUnit_Framework_TestCase
{

	private $moduleName = 'Pixelpromis_NiceAdmin';

	/** @var ObjectManager */
	private $objectManager;

	protected function setUp()
	{
		parent::setUp();

		$this->objectManager = ObjectManager::getInstance();
	}

	public function testModuleIsRegistered()
	{
		$this->assertArrayHasKey($this->moduleName, (new ComponentRegistrar())->getPaths(ComponentRegistrar::MODULE));
	}

	public function testModuleIsEnabledInTestEnviroment()
	{
		/** @var ModuleList $moduleList */
		$moduleList = $this->objectManager->create(ModuleList::class);

		$this->assertTrue($moduleList->has($this->moduleName));
	}

	public function testModuleIsEnabledInRealEnviroment()
	{
		$dirList = $this->objectManager->create(DirectoryList::class, ['root' => BP]);
		$configReader = $this->objectManager->create(DeploymentConfig\Reader::class, ['dirList' => $dirList]);
		$deploymentConfig = $this->objectManager->create(DeploymentConfig::class, ['reader' => $configReader]);

		/** @var ModuleList $moduleList */
		$moduleList = $this->objectManager->create(ModuleList::class, ['config' => $deploymentConfig]);

		$this->assertTrue($moduleList->has($this->moduleName));
	}

}